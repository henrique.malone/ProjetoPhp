<?php
namespace DinfoApp\v1\Controllers;

use PHPUnit\Framework\TestCase;
use DinfoApp\App;
use Slim\Http\Environment;
use Slim\Http\Request;

class LoginTest extends TestCase
{
    protected $app;
    protected $token;

    public function setUp()
    {
        $this->app = (new App())->get();
        $this->token = "";
    }

    public function testDestivaUsuario()
    {
        $dados = array('situacao'=>'0');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'PATCH',
            'REQUEST_URI'    => 'api/v1/usuario/1',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
    }

    /**
     * @depends testDestivaUsuario
     */
    public function testLoginUsuarioDesativado()
    {
        $dados = array('email'=>'matioli@unicamp.br','senha'=>'matioli');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => 'api/v1/login',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 401);

    }

    /**
     * @depends testLoginUsuarioDesativado
     */
    public function testAtivaUsuario()
    {
        $dados = array('situacao'=>'1');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'PATCH',
            'REQUEST_URI'    => 'api/v1/usuario/1',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
    }

    /**
     * @depends testAtivaUsuario
     */
    public function testLoginUsuarioAtivado()
    {
        $dados = array('email'=>'matioli@unicamp.br','senha'=>'matioli');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => 'api/v1/login',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"],"sucesso");

        return $result["token"];
    }

    /**
     * @depends testLoginUsuarioAtivado
     */
     public function testLogoutUsuario(String $token)
    {
        $dados = array('token'=>$token);
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => 'api/v1/logout',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"],"sucesso");
    }
}