<?php
/**
 * Created by PhpStorm.
 * User: professor
 * Date: 18/09/2017
 * Time: 21:53
 */

namespace DinfoApp\v1\Models;

use DinfoApp\Database\PDOConnection;

class Curso
{
    private $id;
    private $nome;

    public function getAll(){
        $resposta = array(
          "code" => 200,
          "status" => "sucesso",
            "data" => array(
                array("sigla"=>"dinfo",
                    "nome" => "Depto Informatica"),
                array("sigla"=>"dmec",
                    "nome" => "Depto Mecânica"),
                array("sigla"=>"denf",
                    "nome" => "Depto Enfermagem")
            )
        );
        return $resposta;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

}