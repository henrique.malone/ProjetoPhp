<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 02/09/2017
 * Time: 11:04
 */

namespace DinfoApp\v1\Models;
use DinfoApp\Database\PDOConnection;

class Usuario
{
    private $id;
    private $email;
    private $documento;
    private $nome;
    private $senha;

    /**
     * Usuarios constructor.
     * @param $id
     * @param $email
     * @param $documento
     * @param $nome
     * @param $senha
     */
    public function __construct()
    {
    }

    public function getAll()
    {
        $conn = PDOConnection::instance()->getConnection();
        $resposta=array();

        $strSql = "SELECT * FROM usuario ";
        $query = $conn->prepare($strSql);
        $query->execute();
        $dados = $query->fetchAll(\PDO::FETCH_ASSOC);
        $resposta['code'] = 200;
        $resposta['status']='sucesso';
        $resposta['data']=$dados;

        return $resposta;
    }

    public function get($id)
    {
        $conn = PDOConnection::instance()->getConnection();
        $resposta=array();

        $strSql = "SELECT * FROM usuario WHERE idUsuario=:id ";
        $query = $conn->prepare($strSql);
        $query->bindParam(':id',$id);

        $query->execute();
        $dados = $query->fetch(\PDO::FETCH_ASSOC);
        if($dados) {
            $resposta['code'] = 200;
            $resposta['status'] = 'sucesso';
            $resposta['data'] = $dados;
        }else{
            $resposta['code'] = 404;
            $resposta['status'] = 'Usuário não encontrado!';
        }
        return $resposta;
    }

    public function create($nome, $email, $doc, $senha)
    {
        try {
            $conn = PDOConnection::instance()->getConnection();
            $response = array();
            $encPwd = md5($senha); // You can use more secure password encryption method
            $sql = "INSERT INTO usuario ";
            $sql .= "(nome, email, senha) ";
            $sql .= "VALUES(:nome, :email, :senha) ";
            $res = $conn->prepare($sql);
            $res->bindParam(':nome', $nome);
            $res->bindParam(':email', $email);
            $res->bindParam(':senha', $encPwd);

            $res->execute();
            if ($res) {
                $LAST_ID = $conn->lastInsertId();
                $response['code'] = 200;
                $response['status'] = 'sucesso';
                $response['data'] = array(
                    'id'=>$LAST_ID,
                    'nome' => $nome,
                    'email' => $email);
            } else {
                $response['code'] = 401;
                $response['status'] = 'falha';
                $resposta['message'] = 'Falha ao criar usuário';
            }
        }catch(\PDOException $pex){
            $response['code']=500;
            $response['status']='falha';
            $response['message'] = 'Erro interno do servidor'; //$pex->getCode()." | ".$pex->getMessage();
        }
        return $response;
    }

    public function update($id, $nome, $email)
    {
        if($id==1){
            $response['code'] = 406;
            $response['status'] = 'Não é permitido a alteração do usuário com ID = 1!';
        }else {

            try {
                $conn = PDOConnection::instance()->getConnection();
                $response = array();
                $sql = "UPDATE usuario ";
                $sql .= " SET nome=:nome, email=:email ";
                $sql .= " WHERE idUsuario=:id ";
                $res = $conn->prepare($sql);
                $res->bindParam(':id', $id);
                $res->bindParam(':nome', $nome);
                $res->bindParam(':email', $email);

                $res->execute();
                if ($res->RowCount() > 0) {
                    $response['code'] = 200;
                    $response['status'] = 'sucesso';
                    $response['data'] = array('nome' => $nome,
                        'email' => $email);
                } else {
                    $response['code'] = 401;
                    $response['status'] = 'Falha ao atualizar usuário!';
                }
            } catch (\PDOException $ex) {
                $response['code'] = 401;
                $response['status'] = 'Falha ao atualizar usuário!';
            }
        }
        return $response;
    }

    public function ativa_desativa($id,$situacao)
    {
        try{
            $conn = PDOConnection::instance()->getConnection();
            $response = array();
            $sql = "UPDATE usuario ";
            $sql .= " SET situacao=:situacao ";
            $sql .= " WHERE idUsuario=:id ";
            $res = $conn->prepare($sql);
            $res->bindParam(':id', $id);
            $res->bindParam(':situacao', $situacao);

            $res->execute();
            if($res->RowCount()>0){
                $response['code'] = 200;
                $response['status'] = 'sucesso';
            }else{
                $response['code'] = 401;
                $response['status'] = 'Falha ao atualizar usuário!';
            }
        }catch(\PDOException $ex){
            $response['code'] = 401;
            $response['status'] = 'Falha ao atualizar usuário!';
            $response['Exception'] = $ex->getCode()." : ".$ex->getMessage();
        }

        return $response;
    }

    public function delete($id)
    {
        if($id==1){
            $response['code'] = 406;
            $response['status'] = 'Não é permitido a exclusão do usuário com ID = 1!';
        }else {
            try {
                $conn = PDOConnection::instance()->getConnection();
                $response = array();
                $sql = "DELETE FROM usuario ";
                $sql .= " WHERE idUsuario=:id ";
                $res = $conn->prepare($sql);
                $res->bindParam(':id', $id);

                $res->execute();
                if ($res->RowCount() > 0) {
                    $response['code'] = 200;
                    $response['status'] = 'sucesso';
                } else {
                    $response['code'] = 401;
                    $response['status'] = 'Falha ao atualizar usuário!';
                }
            } catch (\PDOException $ex) {
                $response['code'] = 401;
                $response['status'] = 'Falha ao atualizar usuário!';
            }
        }
        return $response;
    }

    public function zap(){
        try{
            $conn = PDOConnection::instance()->getConnection();
            $response = array();
            $sql = "DELETE FROM usuario WHERE idUsuario>1; ALTER TABLE usuarios AUTO_INCREMENT = 2 ";
            $conn->query($sql);

            $response['code'] = 200;
            $response['status'] = 'sucesso';
        }catch(\PDOException $ex){
            $response['code'] = 401;
            $response['status'] = 'Falha ao limpar tabela UsuarioTest!';
        }

        return $response;

    }
}