<?php
/**
 * Created by PhpStorm.
 * User: aluno
 * Date: 29/09/2017
 * Time: 21:33
 */

namespace DinfoApp\v1\Models;
use DinfoApp\Database\PDOConnection;


class Grade
{
    private $idGrade;
    private $nome;
    private $situacao;
    private $idTurma;

    public function create($nome, $situacao, $idTurma)
    {
        try {
            $conn = PDOConnection::instance()->getConnection();
            $response = array();
            $sql = "INSERT INTO grade ";
            $sql .= "(nome, situacao, idTurma) ";
            $sql .= "VALUES(:nome, :situacao, :idTurma) ";
            $res = $conn->prepare($sql);
            $res->bindParam(':nome', $nome);
            $res->bindParam(':situacao', $situacao);
            $res->bindParam(':idTurma', $idTurma);

            $res->execute();
            if ($res) {
                $LAST_ID = $conn->lastInsertId();
                $response['code'] = 200;
                $response['status'] = 'sucesso';
                $response['data'] = array(
                    'idGrade'=>$LAST_ID,
                    'nome' => $nome,
                    'situacao' => $situacao,
                    'idTurma' => $idTurma,);
            } else {
                $response['code'] = 401;
                $response['status'] = 'falha';
                $resposta['message'] = 'Falha ao criar a grade';
            }
        }catch(\PDOException $pex){
            $response['code']=500;
            $response['status']='falha';
            $response['message'] = 'Erro interno do servidor'; //$pex->getCode()." | ".$pex->getMessage();
        }
        return $response;
    }

    /**
     * @return mixed
     */
    public function getIdGrade()
    {
        return $this->idGrade;
    }

    /**
     * @param mixed $idGrade
     */
    public function setIdGrade($idGrade)
    {
        $this->idGrade = $idGrade;
    }

    /**
     * @return mixed
     */
    public function getIdTurma()
    {
        return $this->idTurma;
    }

    /**
     * @param mixed $idTurma
     */
    public function setIdTurma($idTurma)
    {
        $this->idTurma = $idTurma;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * @param mixed $situacao
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    }
}