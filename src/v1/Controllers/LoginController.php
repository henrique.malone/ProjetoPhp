<?php
namespace DinfoApp\v1\Controllers;

use \Psr\Http\Message\RequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use DinfoApp\Login\Login;

class LoginController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * Undocumented function
     * @param [object] $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }


    function login(Request $request, Response $response, $args)
    {
        $apiResponse = array();
        $email = $request->getParam('email');
        $senha = $request->getParam('senha');
        if (!empty($email) && !empty($senha)) {
            $login = new Login();
            $apiResponse = $login->login($email, $senha);
        } else {
            $apiResponse['code'] = 401;
            $apiResponse['status'] = 'falha';
            $apiResponse['message'] = 'Por favor, informe os dados necessários!';
        }
        return $response->withJson($apiResponse, $apiResponse['code']);
    }

    function loginWithToken(Request $request, Response $response, $args)
    {
        $apiResponse = array();
        $token = $request->getParam('token');
        if (!empty($token)) {
            $login = new Login();
            $code = 200;
            $apiResponse = $login->loginWithToken($token);
        } else {
            $code = 401;
            $apiResponse['status'] = 'falha';
            $apiResponse['message'] = 'Por favor, informe os dados necessários!';
        }
        return $response->withJson($apiResponse, $code);
    }


    function logout(Request $request, Response $response, $args)
    {
        $apiResponse = array();
        $token = $request->getParam('token');
        $login = new Login();
        if (!empty($token)) {
            $code = 200;
            $apiResponse = $login->logout($token);
        } else {
            $code = 401;
            $apiResponse['status'] = 'falha';
            $apiResponse['message'] = 'Token inválido!';
        }
        return $response->withJson($apiResponse, $code);
    }
}