<?php
/**
 * Created by PhpStorm.
 * User: professor
 * Date: 18/09/2017
 * Time: 21:59
 */

namespace DinfoApp\v1\Controllers;

use DinfoApp\v1\Models\Curso;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CursoController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    function listaCursos(Request $request, Response $response, $args)
    {
        $cursos = new Curso();
        return $response->withJson($cursos->getAll(), 200);
    }
}