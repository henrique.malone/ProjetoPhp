<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 16/09/2017
 * Time: 11:20
 */

namespace DinfoApp\v1\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use DinfoApp\v1\Models\Boletim;

class BoletimController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function listaBoletim(Request $request, Response $response, $args)
    {
        $boletim = new Boletim();
        return $response->withJson($boletim->getAll(), 200);

    }
}