<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 12/09/2017
 * Time: 09:44
 */

namespace DinfoApp;
use Psr7Middlewares\Middleware\TrailingSlash;


class App
{
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $app;
    public function __construct()
    {
        /**
         * Configurações
         */
        $configs = [
            'settings' => [
                'displayErrorDetails' => true,
            ],
        ];

        /**
         * Container Resources do Slim.
         * Aqui dentro dele vamos carregar todas as dependências
         * da nossa aplicação que vão ser consumidas durante a execução
         * da nossa API
         */
        $container = new \Slim\Container($configs);

        $isDevMode = true;

        /**
         * Converte os Exceptions de Erros 405 - Not Allowed
         */
        $container['notAllowedHandler'] = function ($c) {
            return function ($request, $response, $methods) use ($c) {
                return $c['response']
                    ->withStatus(405)
                    ->withHeader('Allow', implode(', ', $methods))
                    ->withHeader('Content-Type', 'Application/json')
                    ->withHeader("Access-Control-Allow-Methods", implode(",", $methods))
                    ->withJson(["message" => "Method not Allowed; Method must be one of: " . implode(', ', $methods)], 405);
            };
        };

        /**
         * Converte os Exceptions de Erros 404 - Not Found
         */
        $container['notFoundHandler'] = function ($container) {
            return function ($request, $response) use ($container) {
                return $container['response']
                    ->withStatus(404)
                    ->withHeader('Content-Type', 'Application/json')
                    ->withJson(['message' => 'Page not found']);
            };
        };


        $this->app = new \Slim\App($container);

        $this->app->add(new TrailingSlash(false));

        /**
         * ROTAS
         */

        /**
         * Grupo dos enpoints iniciados por v1
         */
        $this->app->group('/v1', function() {

            /**
             * Dentro de v1, o recurso /usuario
             */
            $this->group('/usuario', function() {
                $this->get('', '\DinfoApp\v1\Controllers\UsuarioController:listaUsuarios');
                $this->post('', '\DinfoApp\v1\Controllers\UsuarioController:criaUsuario');
                $this->get('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\UsuarioController:listaUsuario');
                $this->put('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\UsuarioController:atualizaUsuario');
                $this->delete('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\UsuarioController:excluiUsuario');
                $this->delete('/zap', '\DinfoApp\v1\Controllers\UsuarioController:zapUsuario');
                $this->patch('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\UsuarioController:ativaUsuario');
            });
            /**
             * Dentro de v1, o recurso /deparamento
             */
            $this->group('/departamento', function() {
                $this->get('', '\DinfoApp\v1\Controllers\DepartamentoController:listaDepartamentos');
                $this->post('', '\DinfoApp\v1\Controllers\DepartamentoController:criaDepartamento');
                $this->get('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DepartamentoController:listaDepartamento');
                $this->put('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DepartamentoController:atualizaDepartamento');
                $this->delete('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DepartamentoController:excluiDepartamento');
            });
            /**
             * Dentro de v1, o recurso /curso
             */
            $this->group('/curso', function() {
                $this->get('', '\DinfoApp\v1\Controllers\CursoController:listaCursos');
                $this->post('', '\DinfoApp\v1\Controllers\CursoController:criaCurso');
                $this->get('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\CursoController:listaCurso');
                $this->put('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\CursoController:atualizaCurso');
                $this->delete('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\CursoController:excluiCurso');
            });
            /**
             * Dentro de v1, o recurso /disciplina
             */
            $this->group('/disciplina', function() {
                $this->get('', '\DinfoApp\v1\Controllers\DisciplinaController:listaDisciplinas');
                $this->post('', '\DinfoApp\v1\Controllers\DisciplinaController:criaDisciplina');
                $this->get('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DisciplinaController:listaDisciplina');
                $this->put('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DisciplinaController:atualizaDisciplina');
                $this->delete('/{id:[0-9]+}', '\DinfoApp\v1\Controllers\DisciplinaController:excluiDisciplina');
            });
            /**
             * Dentro de v1, o recurso /grade
             */
            $this->group('/grade', function() {
                $this->post('', '\DinfoApp\v1\Controllers\GradeController:criaGrade');
            });
            /**
             * Dentro de v1, o recurso /boletim
             */
            $this->group('/boletim', function() {
                $this->get('/{documento:[0-9]+}', '\DinfoApp\v1\Controllers\BoletimController:listaBoletim');
            });

            /**
             * Dentro de v1, o recurso /boletim
             */
            $this->group('/matricula', function() {
                $this->get('/{idturma:[0-9]+}', '\DinfoApp\v1\Controllers\MatriculaController:listaMatriculasTurma');
                $this->post('', '\DinfoApp\v1\Controllers\MatriculaController:criaMatricula');
            });

            /**
             * Dentro de v1, o recurso /distribuicao
             */
            $this->group('/distribuicao', function() {
                $this->post('', '\DinfoApp\v1\Controllers\DistribuicaoController:criaDistribuicao');
            });

            $this->group('/login', function() {
                $this->post('', '\DinfoApp\v1\Controllers\LoginController:login');
            });
            $this->group('/logout', function() {
                $this->post('', '\DinfoApp\v1\Controllers\LoginController:logout');
            });

        });
    }


    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}